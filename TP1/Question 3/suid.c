#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main( void )
{
    printf( "I belong to effective user ID  %d\n", geteuid());
    printf( "I belong to effective group ID %d\n", getegid());
    printf( "I belong to Real user ID %d\n", getuid());
    printf( "I belong to Real group ID %d\n", getgid() );


    FILE* fichier = fopen("mydir/data.txt","r");
    char Buffer[128];
    while (fgets(Buffer,128,fichier))
    printf("%s",Buffer);
    fclose(fichier);
}

