#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pwd.h>
#include "check_pass.h"
#include <crypt.h>

int check_pass(char *filename, char *password,uid_t user_id)
{
    int i =0;
    FILE *fd;
    char *line = NULL;
    char delim[] = ":";
    char *pwd, *uid;
    size_t len = 0;
    char *toCrackCiph;
    int res=1;
    if ((fd = fopen(filename, "r")) != NULL){
        while (getline(&line, &len, fd) != -1){
            uid = strtok(line, delim);
            pwd = strtok(NULL, delim);

            if (atoi(uid) == user_id){

            		toCrackCiph = crypt(password,"sa");
        		    if((strlen(pwd)-1)!=strlen(toCrackCiph)){
                     res =0;
                }else{
                     while(i<strlen(toCrackCiph)){
                         if(toCrackCiph[i]!=pwd[i]){
                             res= 0;
                             i=strlen(toCrackCiph);
                         }
                         i++;
                    }
                    fclose(fd);

                }
          }
        }
    }else{
    printf("Error opening file");
    }

    return res;
}
